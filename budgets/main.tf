terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.90.0"
    }
  }
}

# Configure the Microsoft Azure Provider
provider "azurerm" {
  features {}
}

data "azurerm_resource_group" "rg" {
  name = "${var.resource_group}"
}

resource "azurerm_consumption_budget_resource_group" "budget" {
  name              = var.budget_name
  resource_group_id = data.azurerm_resource_group.rg.id

  amount     = var.budget_amount
  time_grain = "Monthly"

  time_period {
    start_date = "2022-01-01T00:00:00Z"
    end_date   = "2023-07-01T00:00:00Z"
  }

  notification {
    enabled        = true
    threshold      = var.threshold
    operator       = "EqualTo"
    threshold_type = "Forecasted"

    contact_emails = var.contact_emails
  }

  notification {
    enabled   = true
    threshold = var.threshold
    operator  = "GreaterThan"
    threshold_type = "Actual"

    contact_emails = var.contact_emails
  }
}