variable "resource_group" {
    type = string
}
variable "budget_name" {
    type = string
    default = "MyBudget"
}
variable "budget_amount" {
    type = number
    default = 100
}
variable "threshold" {
    type = number
    default = 85
}
variable "start_date" {
    type = string
}
variable "end_date" {
    type = string
}
variable "contact_emails" {
    type = list
    default = [ "test@test.com"]
}